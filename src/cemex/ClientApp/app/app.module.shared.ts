import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpModule } from '@angular/http';

import { AppComponent } from './views/app/app.component';
import { StartComponent } from './views/app/start.view';
import { WelcomeComponent } from './views/welcome/welcome.view';

import { CmxCoreCommonModule, AuthGuard } from '@cemex-core/angular-services-v2/dist';
import { CmxLoginModule } from '@cemex/cmx-login-v4/dist/index';
import { CmxNavHeaderModule } from '@cemex/cmx-nav-header-v4/dist';
import { CmxSidebarModule } from '@cemex/cmx-sidebar-v4/';
import { CmxButtonModule } from '@cemex/cmx-button-v4/dist';
import { InputModule } from '@cemex/cmx-input-v3/dist';
import { CmxDropdownModule } from '@cemex/cmx-dropdown-v1/dist'
import { CmxCheckboxModule } from '@cemex/cmx-checkbox-v1/dist';
import { CmxTableModule } from '@cemex/cmx-table-v1/dist';
import { CmxDialogv3Module } from '@cemex/cmx-dialog-v3/dist';
import { CmxPanelCardModule } from '@cemex/cmx-panel-card-v1/dist';
import { AlertModule } from '@cemex/cmx-alert-v2/dist';

import { HttpClientModule } from '@angular/common/http';

import { TranslationService, FormatterService } from '@cemex-core/angular-localization-v1/dist';

export const sharedConfig: NgModule = {
    providers: [
        { provide: 'TRANSLATION_PRODUCT_PATH', useValue: "angular-boiler" },
        { provide: "TRANSLATION_LANGUAGES", useValue: window["CMX_LANGUAGES"] },
        { provide: "DEFAULT_LANGUAGE_ISO", useValue: "en_US" },
        { provide: "USE_TRANSLATION_SERVER", useValue: true },
        TranslationService,
        FormatterService,
    ],
    imports: [
        BrowserModule,
        CommonModule,
        HttpModule,
        FlexLayoutModule,
        CmxLoginModule,
        
        RouterModule.forRoot([
            { path: "", redirectTo: 'app', pathMatch: 'full' },
            { path: "welcome", component: WelcomeComponent, data: { redirectTo: 'app', } },
            { path: '**', redirectTo: 'app' }
        ])],
    declarations: [
        StartComponent,
        AppComponent,
        WelcomeComponent
    ],
    bootstrap: [StartComponent]
}
export class AppModuleShared {
}
