import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { sharedConfig } from './app.module.shared';
import { FeatureToggleService } from './services/feature.service';
import { AuthGuard, SessionService, HttpCemex, ProjectSettings } from '@cemex-core/angular-services-v2/dist';
import { Broadcaster } from '@cemex-core/events-v1/dist';



@NgModule({
    bootstrap: sharedConfig.bootstrap,
    declarations: sharedConfig.declarations,
    providers: [
        sharedConfig.providers,
        AuthGuard,
        SessionService,
        HttpCemex,
        ProjectSettings,
        Broadcaster,
        // uncomment these lines for the translation server to kick in
        // { provide: 'ORIGIN_URL', useValue: location.origin },
        // { provide: 'USE_TRANSLATION_SERVER', useValue: true },
        // { provide: 'PRODUCT_PATH', useValue:'/translation-service/'},
            { provide: "TRANSLATION_LANGUAGES", useValue: window["CMX_LANGUAGES"] },
            { provide: 'TRANSLATION_PRODUCT_PATH', useValue: "/" },
            { provide: 'USE_TRANSLATION_SERVER', useValue: false},
            { provide: 'USE_LOCAL_MANIFEST', useValue: true },
            { provide: 'USE_RTL', useValue: false },
            { provide: 'PRODUCT_PATH', useValue: '/' },
            FeatureToggleService,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        // HttpModule,
        sharedConfig.imports
    ]
})
export class AppModule {
}
