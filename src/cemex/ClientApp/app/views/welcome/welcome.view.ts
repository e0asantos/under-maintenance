import { Component } from '@angular/core';
import {  SessionService } from '@cemex-core/angular-services-v2/dist';
import { Observable } from 'rxjs/Observable';
import { TranslationService } from "@cemex-core/angular-localization-v1/dist";

@Component({
    selector: 'welcome-component',
    styleUrls: ['./welcome.component.css'],
    templateUrl:'./welcome.view.html'
})
export class WelcomeComponent {

    private myMethod = new Observable<any>((x) => {
        console.log("deleting my_session_storage:", sessionStorage.getItem("my_session_storage") );
        sessionStorage.removeItem("my_session_storage");
    })

    constructor(sessionService:SessionService, translation:TranslationService) {
        sessionService.setBeforeLogout(this.myMethod);
    }

    public randomString(inputString:string):string{
        return "lala";
    }
}
