/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1045);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1045:
/***/ (function(module, exports) {

// google map exemple 1 - simple map
(function(window, google) {



    // The action happens in self invoking function

    // Configure map options

    // ---------------------



    var options = {

            center: {

                lat: 25.686614,

                lng: -100.316113

                //to get the latitude and longitude use: http://www.latlong.net/

            },

            zoom: 12,

            styles: [{
                    "featureType": "administrative.country",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "visibility": "on"
                    }]
                },
                {
                    "featureType": "administrative.province",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "visibility": "on"
                    }]
                },
                {
                    "featureType": "administrative.province",
                    "elementType": "labels.text",
                    "stylers": [{
                        "visibility": "on"
                    }]
                }
            ], // snazzymaps.com

            zoomControl: true,

            mapTypeControl: false,

            scaleControl: false,

            streetViewControl: false,

            rotateControl: false,

            fullscreenControl: false,

            scrollwheel: false,

            draggable: true,

            zoomControlOptions: {

                position: google.maps.ControlPosition.TOP_RIGHT

            },

            mapTypeControlOptions: {

                position: google.maps.ControlPosition.TOP_CENTER

            }

        },


        // A div with the id #map was created before in the HTML document

        // ---------------------------------------------------------------------

        element = document.getElementById('map'),


        // Placing the map in the selected div above

        // -----------------------------------------

        map = new google.maps.Map(element, options);



}(window, google));


// google map example 2 - bottom ribbon
(function(window, google) {



    // The action happens in self invoking function

    // Configure map options

    // ---------------------



    var options = {

            center: {

                lat: 25.686614,

                lng: -100.316113

                //to get the latitude and longitude use: http://www.latlong.net/

            },

            zoom: 12,

            styles: [{
                    "featureType": "administrative.country",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "visibility": "on"
                    }]
                },
                {
                    "featureType": "administrative.province",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "visibility": "on"
                    }]
                },
                {
                    "featureType": "administrative.province",
                    "elementType": "labels.text",
                    "stylers": [{
                        "visibility": "on"
                    }]
                }
            ], // snazzymaps.com

            zoomControl: true,

            mapTypeControl: false,

            scaleControl: false,

            streetViewControl: false,

            rotateControl: false,

            fullscreenControl: false,

            scrollwheel: false,

            draggable: true,

            zoomControlOptions: {

                position: google.maps.ControlPosition.TOP_RIGHT

            },

            mapTypeControlOptions: {

                position: google.maps.ControlPosition.TOP_CENTER

            }

        },


        // A div with the id #map was created before in the HTML document

        // ---------------------------------------------------------------------

        element = document.getElementById('map2'),


        // Placing the map in the selected div above

        // -----------------------------------------

        map = new google.maps.Map(element, options);



}(window, google));


// google map example 3 - side panel
(function(window, google) {



    // The action happens in self invoking function

    // Configure map options

    // ---------------------



    var options = {

            center: {

                lat: 25.686614,

                lng: -100.316113

                //to get the latitude and longitude use: http://www.latlong.net/

            },

            zoom: 12,

            styles: [{
                    "featureType": "administrative.country",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "visibility": "on"
                    }]
                },
                {
                    "featureType": "administrative.province",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "visibility": "on"
                    }]
                },
                {
                    "featureType": "administrative.province",
                    "elementType": "labels.text",
                    "stylers": [{
                        "visibility": "on"
                    }]
                }
            ], // snazzymaps.com

            zoomControl: true,

            mapTypeControl: false,

            scaleControl: false,

            streetViewControl: false,

            rotateControl: false,

            fullscreenControl: false,

            scrollwheel: false,

            draggable: true,

            zoomControlOptions: {

                position: google.maps.ControlPosition.TOP_RIGHT

            },

            mapTypeControlOptions: {

                position: google.maps.ControlPosition.TOP_CENTER

            }

        },


        // A div with the id #map was created before in the HTML document

        // ---------------------------------------------------------------------

        element = document.getElementById('map3'),


        // Placing the map in the selected div above

        // -----------------------------------------

        map = new google.maps.Map(element, options);



}(window, google));


// google map dashboard module example
(function(window, google) {



    // The action happens in self invoking function

    // Configure map options

    // ---------------------



    var options = {

            center: {

                lat: 25.686614,

                lng: -100.316113

                //to get the latitude and longitude use: http://www.latlong.net/

            },

            zoom: 12,

            styles: [{
                    "featureType": "administrative.country",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "visibility": "on"
                    }]
                },
                {
                    "featureType": "administrative.province",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "visibility": "on"
                    }]
                },
                {
                    "featureType": "administrative.province",
                    "elementType": "labels.text",
                    "stylers": [{
                        "visibility": "on"
                    }]
                }
            ], // snazzymaps.com

            zoomControl: true,

            mapTypeControl: false,

            scaleControl: false,

            streetViewControl: false,

            rotateControl: false,

            fullscreenControl: false,

            scrollwheel: false,

            draggable: true,

            zoomControlOptions: {

                position: google.maps.ControlPosition.TOP_RIGHT

            },

            mapTypeControlOptions: {

                position: google.maps.ControlPosition.TOP_CENTER

            }

        },


        // A div with the id #map was created before in the HTML document

        // ---------------------------------------------------------------------

        element = document.getElementById('map4'),


        // Placing the map in the selected div above

        // -----------------------------------------

        map = new google.maps.Map(element, options);



}(window, google));

/***/ })

/******/ });