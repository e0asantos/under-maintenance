import 'jquery';
import './lib/prism/prism.js';

require('./lib/prism/prism.css')
var css = require('../scss/app.scss');

// A $( document ).ready() block.
$(document).ready(function() {

    // the toggle 
    $('.collapse-trigger').on("click", function() {
        if ($(this).hasClass('opened-nav')) {
            //$('#wrapper').find('.navbar-static-side').css("display", "none");
            $('#wrapper').find('.navbar-static-side').animate({ marginLeft: '-160px' }, 300);
            //$('#wrapper').find('.page-wrapper').css('margin-left', '0');
            $('#wrapper').find('.page-wrapper').animate({ marginLeft: '0px' }, 300);
            //$(this).removeClass("opened-nav").addClass("hidden-nav");
            $(this).css("display", "none");
            $('#wrapper').find('.expand-trigger').css("display", "block");
        }
    });

    $('.expand-trigger').on("click", function() {
        if ($(this).hasClass('hidden-nav')) {
            //$('#wrapper').find('.navbar-static-side').css("display", "block");
            $('#wrapper').find('.navbar-static-side').animate({ marginLeft: '0px' }, 300);
            //$('#wrapper').find('.page-wrapper').css('margin-left', '160px');
            $('#wrapper').find('.page-wrapper').animate({ marginLeft: '+160px' }, 300);
            //$(this).removeClass("hidden-nav").addClass("opened-nav");
            $('#wrapper').find('.expand-trigger').css("display", "none");
            $('#wrapper').find('.collapse-trigger').css("display", "block");
        }
    });

    // show hide notifications in main navigation
    $('.nav-notifications-item').on("click", function(event) {
        // -- || -- the transition should be smooth, with a fade in / drop animation
        var clickedInside = $(event.target).hasClass('notifications-container') || $(event.target).parents('.notifications-container').length > 0;
        if (!clickedInside) {
            $(this).find('.notifications-container').toggle();
        }
    });

    $('html').on('click', function(event) {
        var clickedInside = $(event.target).parents('.nav-notifications-item').length > 0;
        if (!clickedInside) {
            $(this).find('.notifications-container').hide();
        }
    });

    //show search in main navigation
    $('.main-nav-item .icon-magnifier-glass').on("click", function() {
        // not exact behaviour we are after - the transition should be smooth
        $(this).parent().find('.search-holder').show();
        $(this).closest('.main-nav-bar').find('.icon-magnifier-glass').first().hide();
        $(this).closest('.main-nav-bar').find('.nav-notifications-item').hide();
        $(this).closest('.main-nav-bar').find('.vertical-bar').first().hide();
    });

    //hide search in main navigation
    $('.dismiss-search').on("click", function() {
        $(this).parent().hide();
        $(this).closest('.main-nav-bar').find('.icon-magnifier-glass').first().show();
        $(this).closest('.main-nav-bar').find('.nav-notifications-item').show();
        $(this).closest('.main-nav-bar').find('.vertical-bar').first().show();
    });

    // show hide user-logged-container in the main navigation
    $('.user-logged-item').on("click", function() {
        // not exact behaviour we are after - the dismiss should be if clicking outside the container
        // -- || -- the transition should be smooth, with a fade in / drop animation
        $(this).find('.user-logged-container').toggle();
    });

    // keeping active the number field on hover over plus minus btns
    $('.number-field-btn.minus-btn').on("mouseenter", function() {
        $(this).parent().find('.number-control').addClass("active");
    });

    $('.number-field-btn.minus-btn').on("mouseleave", function() {
        $(this).parent().find('.number-control').removeClass("active");
    });

    $('.number-field-btn.minus-btn').on('click', function(event) {
        var numberElement = $(this).parent().find('.number-control').get(0);
        numberElement.stepDown();
    });

    $('.number-field-btn.plus-btn').on("mouseenter", function() {
        $(this).parent().find('.number-control').addClass("active");
    });

    $('.number-field-btn.plus-btn').on("mouseleave", function() {
        $(this).parent().find('.number-control').removeClass("active");
    });

    $('.number-field-btn.plus-btn').on('click', function(event) {
        var numberElement = $(this).parent().find('.number-control').get(0);
        numberElement.stepUp();
    });


    // Syntax highlight code (Prism)
    $('[data-highlight]').each(function(index, element) {
        var container = $('<div></div>').appendTo(element);

        $(element).find('pre').each(function(i, e) {
            i === 0 ? $(e).show() : $(e).hide();

            var lang = $(e).attr('data-lang');
            var link = $('<a href="">' + lang + '</a>').appendTo(container);

            container.css('text-align', 'right');
            link.css('padding', '0');
            link.css('font-size', '.9em');
            link.css('padding-right', '.66em');
            link.click(function(event) {
                event.preventDefault();
                $(element).find('[data-lang]').hide();
                $(element).find('[data-lang="' + lang + '"]').show();
            });
        });
    });

});