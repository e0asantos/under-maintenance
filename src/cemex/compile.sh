CYAN='\033[0;36m'
NC='\033[0m' # No Color

if [ "$1" == "clean" ]; then
    printf "Cleaning ${CYAN}.NET${NC} files\n"
    rm -rf bin/
    rm -rf obj/

    printf "Cleaning ${CYAN}npm modules${NC}\n"
    rm -rf node_modules/

    printf "Cleaning ${CYAN}webpack${NC} dist\n"
    rm -rf ClientApp/dist/
    rm -rf wwwroot/dist/
fi

# Default dev
export API_ENV=""
export API_HOST_MW=https://cemexqas.azure-api.net/
#export API_HOST=/xserver/
export API_HOST=https://cemexqas.azure-api.net/
export API_HOST_FULL=https://cemexqas.azure-api.net/
export API_ORG=""
export APP_CODE=OrderProductCat_App
export CLIENT_ID=dd2ee55f-c93c-4c1b-b852-58c18cc7c277
export DUMMY_KEY_FOR_LOCAL=yes
export TRANSLATE_URL=http://server-status.mybluemix.net/
export BASE_URL=NGX-BOILERPLATE/

if [ "$1" == "quality" ]; then
    export API_HOST=https://api.us2.apiconnect.ibmcloud.com/
    export API_HOST_FULL=https://api.us2.apiconnect.ibmcloud.com/cnx-gbl-org-quality/quality/
    export API_ORG=cnx-gbl-org-quality/
    export API_ENV=quality/
    export APP_CODE=OrderProductCat_App
    export CLIENT_ID=721e5c7b-73b8-40e1-8cb2-31c7dbdbd1be
fi

if [ "$1" == "production" ]; then
    export API_HOST=https://api.us2.apiconnect.ibmcloud.com/
    export API_HOST_FULL=https://api.us2.apiconnect.ibmcloud.com/cnx-gbl-org-production/production/
    export API_ORG=cnx-gbl-org-production/
    export API_ENV=production/
    export APP_CODE=OrderProductCat_App
    export CLIENT_ID=ba4ae518-258b-4b29-9d6f-8d77ab1cef7a
fi

if [ "$1" == "full" ]; then
    printf "Cleaning ${CYAN}dls submodule${NC} dist\n"
rm -rf submodules/dls/node_modules

printf ".NET Environment: ${CYAN}Development${NC}\n"
export ASPNETCORE_ENVIRONMENT=Development

printf "Restoring .NET ${CYAN}project.json${NC} dependencies\n"
dotnet restore

printf "Restoring npm ${CYAN}package.json${NC} dependencies\n"
npm install

printf "Compiling with ${CYAN}webpack${NC}\n"
webpack --config webpack.config.vendor.js
webpack --config webpack.config.js


fi
printf "Running .NET ${CYAN}server${NC}\n"
dotnet run