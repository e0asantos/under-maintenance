using System;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;


using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Chroniton;
using Chroniton.Jobs;
using Chroniton.Schedules;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Net.Http.Headers;


namespace angularX.Controllers
{
    public class HomeController : Controller

    {
        private readonly ILogger _logger;
        private HttpClient client;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
            var handler = new HttpClientHandler();
            handler.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            client=new HttpClient(handler);
        }

        public async Task<IActionResult> Index()
        {
            var translate = new TranslateController();
            var languages = await translate.LoadLanguagesFromServer();
            Console.WriteLine(languages);
            ViewData["CMX_LANGUAGES"] = languages;
            return View();

            // var requestBodyStream = new MemoryStream();
            // var originalRequestBody = this.HttpContext.Request.Body;

            // this.HttpContext.Request.Body.CopyToAsync(requestBodyStream);
            // requestBodyStream.Seek(0, SeekOrigin.Begin);


            // var requestBodyText = new StreamReader(requestBodyStream).ReadToEnd();
            // Console.WriteLine(requestBodyText);
            



            // string requestQueryString = this.HttpContext.Request.QueryString.ToString();
            // Console.WriteLine(this.HttpContext.Request.Body);
            // Console.WriteLine(this.HttpContext.Request.ContentType);
            // Console.WriteLine(this.HttpContext.Request.Path);
            // Console.WriteLine(this.HttpContext.Request.Protocol);
            // Console.WriteLine(this.HttpContext.Request.Headers);
            // Console.WriteLine(this.HttpContext.Request.Query);
            // Console.WriteLine(this.HttpContext.Request.QueryString.Value);
            
            // string realPath = this.HttpContext.Request.Path.ToString().IndexOf("xserver") != -1 ? this.HttpContext.Request.Path.ToString().Substring(8) : this.HttpContext.Request.Path.ToString();
            // if (this.HttpContext.Request.Method == "POST" && this.HttpContext.Request.Path.ToString().IndexOf("xserver") != -1)
            // {
            //     string statusCode = string.Empty;
            //     StringContent dataToProxy = new StringContent(requestBodyText, Encoding.UTF8, this.HttpContext.Request.ContentType);
            //     if (HttpContext.Session.GetString("isUserLoggedIn") != null)
            //     {
            //         dataToProxy.Headers.Add("X-IBM-Client-Id", Environment.GetEnvironmentVariable("CLIENT_ID"));
            //         dataToProxy.Headers.Add("App-Code", Environment.GetEnvironmentVariable("APP_CODE"));
            //         dataToProxy.Headers.Add("jwt", HttpContext.Session.GetString("jwt"));
            //         dataToProxy.Headers.Add("Authorization", HttpContext.Session.GetString("Authorization"));
            //         dataToProxy.Headers.Add("Accept-Language", this.HttpContext.Request.Headers["Accept-Language"].ToString());
                    
            //     }

            //     using (HttpResponseMessage response = await client.PostAsync(Environment.GetEnvironmentVariable("API_HOST_MW") + realPath + this.HttpContext.Request.QueryString.Value, dataToProxy))
            //     {
            //         response.EnsureSuccessStatusCode();
            //         bool containsTranslatorRole = false;
            //         if (response.IsSuccessStatusCode)
            //         {
            //             HttpContent responseContent = response.Content;
            //             using (var reader = new StreamReader(await responseContent.ReadAsStreamAsync()))
            //             {
            //                 String jsonString = await reader.ReadToEndAsync();
            //                 //Here I would like to do a JSON Convert of my variable result
            //                 if (realPath.IndexOf("/secm/oam/oauth2/token") != -1)
            //                 {
            //                     CemexAuth cmxAuth = JsonConvert.DeserializeObject<CemexAuth>(jsonString);
            //                     Console.WriteLine("=======");
            //                     /**
                            
            //                 Uncomment these lines if you want to verify the role is specific to the current application
                            
            //                  */
            //                     // foreach (var item in cmxAuth.applications)
            //                     // {
            //                     //     Console.WriteLine(item.applicationName);
            //                     //     foreach (var role in item.roles)
            //                     //     {
            //                     //         if (role.roleCode == RoleConstants.TranslatorRole)
            //                     //         {
            //                     //             HttpContext.Session.SetString("isUserLoggedIn", "yes");
            //                     //             HttpContext.Session.SetString("Authorization", cmxAuth.oauth2.access_token);
            //                     //             HttpContext.Session.SetString("jwt", cmxAuth.jwt);
            //                     //             containsTranslatorRole = true;
            //                     //         }
            //                     //     }
            //                     // }
            //                     // if (containsTranslatorRole)
            //                     // {
            //                     //     return Json(JsonConvert.DeserializeObject(jsonString));
            //                     // }
            //                     // else
            //                     // {
            //                     //     return NotFound("Not authorized to use this application");
            //                     // }
            //                 }

            //                 return Json(JsonConvert.DeserializeObject(jsonString));

            //             }
            //         }
            //         else
            //         {
            //             statusCode = response.StatusCode.ToString();
            //             return NotFound(response.StatusCode.ToString());
            //         }
            //     }
            // }
            // else if (this.HttpContext.Request.Method == "GET" && this.HttpContext.Request.Path.ToString().IndexOf("xserver") != -1)
            // {
            //     string statusCode = string.Empty;
            //     StringContent dataToProxy = new StringContent(requestBodyText, Encoding.UTF8, this.HttpContext.Request.ContentType);
            //     if (HttpContext.Session.GetString("isUserLoggedIn") != null)
            //     {
            //         client.DefaultRequestHeaders.Add("X-IBM-Client-Id", Environment.GetEnvironmentVariable("CLIENT_ID").ToString());
            //         client.DefaultRequestHeaders.Add("App-Code", Environment.GetEnvironmentVariable("APP_CODE").ToString());
            //         client.DefaultRequestHeaders.Add("jwt", HttpContext.Session.GetString("jwt").ToString());
            //         client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", HttpContext.Session.GetString("Authorization").ToString());
            //         client.DefaultRequestHeaders.Add("Accept-Language", this.HttpContext.Request.Headers["Accept-Language"].ToString());
            //     }
            //     using (HttpResponseMessage response = await client.GetAsync(Environment.GetEnvironmentVariable("API_HOST_MW") + realPath + this.HttpContext.Request.QueryString.Value))
            //     {
                    
            //         response.EnsureSuccessStatusCode();
            //         if (response.IsSuccessStatusCode)
            //         {
            //             HttpContent responseContent = response.Content;
            //             using (var reader = new StreamReader(await responseContent.ReadAsStreamAsync()))
            //             {
            //                 String jsonString = await reader.ReadToEndAsync();
            //                 //Here I would like to do a JSON Convert of my variable result
            //                 var proxiedData = JsonConvert.DeserializeObject(jsonString);
            //                 return Json(proxiedData);
            //             }
            //         }
            //         else
            //         {
            //             statusCode = response.StatusCode.ToString();
            //             return NotFound(response.StatusCode.ToString());
            //         }
            //     }
            // } else if(this.HttpContext.Request.Method == "GET" && this.HttpContext.Request.Path.ToString().IndexOf("translate") != -1){
            //     string statusCode = string.Empty;
            //     StringContent dataToProxy = new StringContent(requestBodyText, Encoding.UTF8, this.HttpContext.Request.ContentType);
               
            //     using (HttpResponseMessage response = await client.GetAsync(Environment.GetEnvironmentVariable("TRANSLATE_URL")+"api/Translation/" + this.HttpContext.Request.Path.ToString().Substring(11)+ this.HttpContext.Request.QueryString.Value))
            //     {
            //         //Console.WriteLine(Environment.GetEnvironmentVariable("TRANSLATE_URL")+"api/Translation/" + this.HttpContext.Request.Path.ToString().Substring(11)+ this.HttpContext.Request.QueryString.Value);   
            //         response.EnsureSuccessStatusCode();
            //         if (response.IsSuccessStatusCode)
            //         {
            //             HttpContent responseContent = response.Content;
            //             using (var reader = new StreamReader(await responseContent.ReadAsStreamAsync()))
            //             {
            //                 String jsonString = await reader.ReadToEndAsync();
            //                 //Here I would like to do a JSON Convert of my variable result
            //                 var proxiedData = JsonConvert.DeserializeObject(jsonString);
            //                 return Json(proxiedData);
            //             }
            //         }
            //         else
            //         {
            //             statusCode = response.StatusCode.ToString();
            //             return NotFound(response.StatusCode.ToString());
            //         }
            //     }
            // }
            // return View();
        }


        public IActionResult Error()
        {
            return View();
        }
    }
}
