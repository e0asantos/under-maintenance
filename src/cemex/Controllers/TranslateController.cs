
using System;
using System.Net;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.EntityFrameworkCore;

namespace angularX.Controllers
{

    [Route("[controller]")]
    public class TranslateController : Controller
    {
        private HttpClient client;

        public TranslateController()
        {
            var handler = new HttpClientHandler();
            handler.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            client = new HttpClient(handler);
        }

        public void setHeaders()
        {
            if (HttpContext.Session.GetString("isUserLoggedIn") != null)
            {
                client.DefaultRequestHeaders.Add(
                    "X-IBM-Client-Id", Environment.GetEnvironmentVariable("CLIENT_ID").ToString()
                );
                client.DefaultRequestHeaders.Add(
                    "App-Code", Environment.GetEnvironmentVariable("APP_CODE").ToString()
                );
                client.DefaultRequestHeaders.Add(
                    "jwt", HttpContext.Session.GetString("jwt").ToString()
                );
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                    "Bearer", HttpContext.Session.GetString("Authorization").ToString()
                );
                client.DefaultRequestHeaders.Add(
                    "Accept-Language", HttpContext.Request.Headers["Accept-Language"].ToString()
                );
            }
        }



        [HttpGet("GetLanguages")]
        public async Task<string> GetLanguages()
        {
            this.setHeaders();
            return await this.LoadLanguagesFromServer();
        }
        public async Task<string> LoadLanguagesFromServer()
        {
            try
            {
                using (HttpResponseMessage response = await client.GetAsync(Environment.GetEnvironmentVariable("TRANSLATE_URL") + "translate/getLanguages"))
                {
                    if (!response.IsSuccessStatusCode)
                        return LoadLanguagesFromFile();

                    using (var reader = new StreamReader(await response.Content.ReadAsStreamAsync(), System.Text.Encoding.UTF8))
                        return await reader.ReadToEndAsync();
                }
            }
            catch (Exception ex)
            {
                return LoadLanguagesFromFile();
            }
        }
        public string LoadLanguagesFromFile()
        {
            using (var reader = new StreamReader(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/angular-boiler/vendor", "locale-languages.json")))
            {
                var line = reader.ReadToEnd();
                reader.Close();
                return line;
            }
        }


        [HttpGet("/translate/translate/{product}/{iso}")]
        public async Task<string> GetTranslations(string product, string iso)
        {
            this.setHeaders();
            return await this.LoadTranslationsFromServer(product, iso);
        }
        public async Task<string> LoadTranslationsFromServer(string product, string iso)
        {
            try
            {
                using (HttpResponseMessage response = await client.GetAsync(Environment.GetEnvironmentVariable("TRANSLATE_URL") + "translate/translate/" + product + "/" + iso))
                {
                    if (!response.IsSuccessStatusCode)
                        return this.LoadTranslationsFromFile(iso);

                    using (var reader = new StreamReader(await response.Content.ReadAsStreamAsync(), System.Text.Encoding.UTF8))
                        return await reader.ReadToEndAsync();
                }
            }
            catch (Exception ex)
            {
                return this.LoadTranslationsFromFile(iso);
            }
        }
        public string LoadTranslationsFromFile(string iso)
        {
            using (var reader = new StreamReader(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/angular-boiler/vendor", "locale-" + iso + ".json")))
            {
                var line = reader.ReadToEnd();
                reader.Close();
                return line;
            }
        }
    }
}