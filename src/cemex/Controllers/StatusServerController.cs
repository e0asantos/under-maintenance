using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace angularX.Controllers
{
    [Route("api/[controller]")]
    public class StatusServerController : Controller
    {
        

        // GET: api/statusServer/ping?projectName=foo
        [HttpPost("[action]")]
        public JsonResult ping([FromBody] StatusServerModel DigitalServiceName)
        {
            DateTime timeSnapshot= DateTime.Now;
            var data = new { ReceivedAt = timeSnapshot.ToString("o"),from=DigitalServiceName.DigitalServiceName };
            return Json(data);
        }

        [HttpPost("[action]")]
        public string JsonStringBody([FromBody] string content)
        {
            return content;
        }
    }

    public class StatusServerModel  
    {
        public string DigitalServiceName { get; set; }
        public string TechnicalServiceName { get; set; }

    }
}
