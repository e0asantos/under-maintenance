This repository contains a submodule, make sure you clone it with the --recursive flag

git clone https://bitbucket.org/cemex/ngxboilerplate.git --recursive

# Getting started with angular4 Boilerplate
The next steps are required to make this project up and running

- Install the latest .net core runtime (The runtime used here was 2.0 and sdk 2)
- https://github.com/dotnet/core/blob/master/release-notes/download-archives/2.0.0-download.md
- make sure the "dotnet" command runs smoothly on your computer
- make sure you have yarn installed
- make sure you have webpack installed
- make sure you have npm installed
- make sure you have nodejs installed



## How to run the project

This project is run in 3 steps:

* cd into src/cemex
* compile webpack vendor: webpack --config webpack.config.vendor.js
* compile webpack user typescript: webpack (yes the webpack command only)
* dotnet run

OR

* go to src/cemex
* ./compile.sh full


## 1. Clone the boilerplate app with --recursive

When you clone this repository you have to make sure to change 1 important file:


  ```
1)manifest.yml
  ```

The manifest tell bluemix what is the name of the application and what is the container, change this value


## 2. NPMRC for downloading components from repository

All the components under the registry @cemex must be downloaded with the next .npmrc content

https://cemexpmo.atlassian.net/wiki/spaces/SDTS/pages/21877858/NPM+keys

That file must be placed in the root of the folder so that " yarn install " or "npm install" can have read access


## 3. Configure the Env vars

In the file compile.sh you will notice you need several env vars, this project makes use of a .net controller to proxy http calls, make sure you configure the variables propperly according to the document:
https://cemexpmo.atlassian.net/wiki/spaces/SDTS/pages/21933393/Environment+variables+in+Bluemix+containers